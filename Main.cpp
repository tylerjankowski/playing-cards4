#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit {
	CLUBS,
	HEARTS,
	SPADES,
	DIAMONDS
};

enum Rank {
	ACE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING
};


struct Card {
	Suit suit;
	Rank rank;
};

void PrintCard(Card card) {
	cout << "Rank: " << card.rank << " Suit: " << card.suit << "\n";
}

Card HighCard(Card a, Card b) {
	if (a.rank > b.rank) {
		return a;
	}
	return b;
}

int main() {

	Card first;
	first.rank = ACE;
	first.suit = DIAMONDS;

	Card second;
	second.rank = KING;
	second.suit = SPADES;


	PrintCard(first);
	PrintCard(second);

	PrintCard(HighCard(first, second));

	_getch();
	return 1;
}